
_FONT _LOADFONT("C:\windows\fonts\cour.ttf", 20, "MONOSPACE")

RESTORE Microsoft_windows_cp1251
FOR ASCIIcode = 128 TO 255
    READ unicode
    _MAPUNICODE unicode TO ASCIIcode
NEXT

'Для отрисовки элементов интерфейса заменяем первые 11 символов Microsoft_windows_cp1251
'на псевдосимволы из cp866, поскольку в cp1251 этих символов нет.
'Кириллическая А соответствует ASCII 192
Microsoft_windows_cp1251:
DATA 9553,9552,9571,9559,9565,9562,9556,9577,9574,9568,9580,8249,1034,1036,1035,1039
DATA 1106,8216,8217,8220,8221,8226,8211,8212,0,8482,1113,8250,1114,1116,1115,1119
DATA 160,1038,1118,1032,164,1168,166,167,1025,169,1028,171,172,173,174,1031
DATA 176,177,1030,1110,1169,181,182,183,1105,8470,1108,187,1112,1029,1109,1111
DATA 1040,1041,1042,1043,1044,1045,1046,1047,1048,1049,1050,1051,1052,1053,1054,1055
DATA 1056,1057,1058,1059,1060,1061,1062,1063,1064,1065,1066,1067,1068,1069,1070,1071
DATA 1072,1073,1074,1075,1076,1077,1078,1079,1080,1081,1082,1083,1084,1085,1086,1087
DATA 1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103


DECLARE SUB levelgen ()
DECLARE SUB gamescreen ()
DECLARE SUB parser ()
DECLARE SUB screentext ()
DECLARE SUB parscom ()
DECLARE SUB sidescreen ()
DECLARE SUB loadgame ()
DECLARE SUB screenmap ()


TYPE location
    grid AS STRING * 1
    id AS STRING * 1
    curloc AS STRING * 1 'id текущей локации
    locname AS STRING * 39 'название локации
    playermap AS STRING * 1 'карта, которую видит игрок
END TYPE

COMMON SHARED parstext$
COMMON SHARED curstring
COMMON SHARED sidemode
COMMON SHARED gamename$ 'название игры
COMMON SHARED levelname$ 'название уровня
COMMON SHARED xmap 'переменная для движения по карте
COMMON SHARED ymap 'переменная для движения по карте
COMMON SHARED histnum 'число валидных команд в истории команд
COMMON SHARED gameturn 'счетчик ходов
COMMON SHARED pgupdn 'счетчик пролистывания страниц: + вверх, - вниз
COMMON SHARED maxstring 'максимальная строка в файле log.txt
DIM SHARED location(0 TO 22, 2 TO 22, level) AS location
DIM SHARED comhist(11) AS STRING 'история пользовательских команд на 10 команд

sidemode = 1 '1-управление, 2-инвентарь, 3-легенда, 4-статистика
gameturn = 0
xmap = 12 'переменные для рабочей карты
ymap = 10



CALL levelgen
CALL loadgame
CALL gamescreen
CALL sidescreen
CALL parser

SUB gamescreen
CLS

CALL screenmap

'Создаем игровой экран

COLOR 14

'Горизонтальные строки'

LOCATE 1, 2: PRINT STRING$(78, CHR$(129)) '= (9552 unicode, 129 ASCII)
LOCATE 3, 2: PRINT STRING$(78, CHR$(129))
LOCATE 21, 2: PRINT STRING$(17, CHR$(129))
LOCATE 23, 2: PRINT STRING$(60, CHR$(129))
LOCATE 25, 2: PRINT STRING$(78, CHR$(129));

'Вертикальные строки'
FOR y = 2 TO 24
    LOCATE y, 1: PRINT CHR$(128); '| (9553 unicode, 128 ASCII)'
    LOCATE y, 19: PRINT CHR$(128);
    LOCATE y, 62: PRINT CHR$(128);
    LOCATE y, 80: PRINT CHR$(128);
NEXT y

'Отдельные элементы'
LOCATE 3, 80: PRINT CHR$(130); '-|'
LOCATE 21, 19: PRINT CHR$(130)
LOCATE 23, 62: PRINT CHR$(130)

LOCATE 1, 80: PRINT CHR$(131) 'верхний правый угол (9559 unicode, 131 ASCII)'

LOCATE 25, 80: PRINT CHR$(132); 'нижний правый угол (9565 unicode, 132 ASCII)'


LOCATE 25, 1: PRINT CHR$(133); 'нижний левый угол (9562 unicode, 133 ASCII)'

LOCATE 1, 1: PRINT CHR$(134) 'верхний левый угол (9556 unicode, 134 ASCII)'

LOCATE 25, 19: PRINT CHR$(135); '_L (9577 unicode, 135 ASCII)'
LOCATE 25, 62: PRINT CHR$(135);

LOCATE 1, 19: PRINT CHR$(136) 'T (9574 unicode, 136 ASCII)'
LOCATE 1, 62: PRINT CHR$(136)

LOCATE 3, 1: PRINT CHR$(137) '|- (9568 unicode, 137 ASCII) '
LOCATE 21, 1: PRINT CHR$(137)
LOCATE 23, 1: PRINT CHR$(137)

LOCATE 3, 19: PRINT CHR$(138) '+ (9580 unicode, 138 ASCII)'
LOCATE 3, 62: PRINT CHR$(138)
LOCATE 23, 19: PRINT CHR$(138)

'Заголовки'
COLOR 11
LOCATE 1, 35: PRINT "КвестМастер"
COLOR 15
LOCATE 2, 3: PRINT levelname$
LOCATE 22, 2: PRINT " А-Назв. локации"
LOCATE 2, 29: PRINT gamename$

'Управляющие элементы'
LOCATE 2, 2: PRINT CHR$(17) '<-'
LOCATE 2, 18: PRINT CHR$(16) '->'
LOCATE 22, 2: PRINT CHR$(17)
LOCATE 22, 18: PRINT CHR$(16)

COLOR 8
LOCATE 22, 21: PRINT "Выходы: С, З, В, Ю, СВ, ЮВ, ЮЗ, СЗ"


END SUB

SUB levelgen
RANDOMIZE TIMER
locID = 192
FOR y = 2 TO 18
    FOR x = 4 TO 20
        location(x, y, level).grid = "."
        location(x, y, level).playermap = "." 'пользовательская карта для screenmap '
NEXT x, y
location(12, 10, level).grid = CHR$(locID)
location(12, 10, level).id = CHR$(locID)
location(12, 10, level).curloc = CHR$(locID)

FOR k = 1 TO 15
    locID = locID + 1

    DO
        seedX = INT(RND * 12) + 6
        seedY = INT(RND * 12) + 4
        IF location(seedX, seedY, level).grid = "." THEN
            IF (ASC(location(seedX + 2, seedY, level).grid) > 191 OR ASC(location(seedX - 2, seedY, level).grid) > 191 OR ASC(location(seedX, seedY + 2, level).grid) > 191 OR ASC(location(seedX, seedY - 2, level).grid) > 191) THEN
                location(seedX, seedY, level).grid = CHR$(locID)
                location(seedX, seedY, level).id = CHR$(locID)
                EXIT DO
            END IF
        END IF
    LOOP

NEXT k

'Рисуем соединительные линии'

FOR seedX = 6 TO 18
    FOR seedY = 4 TO 16
        IF ASC(location(seedX, seedY, level).grid) > 127 AND ASC(location(seedX + 2, seedY, level).grid) > 127 THEN location(seedX + 1, seedY, level).grid = "|"
        IF ASC(location(seedX, seedY, level).grid) > 127 AND ASC(location(seedX - 2, seedY, level).grid) > 127 THEN location(seedX - 1, seedY, level).grid = "|"
        IF ASC(location(seedX, seedY, level).grid) > 127 AND ASC(location(seedX, seedY + 2, level).grid) > 127 THEN location(seedX, seedY + 1, level).grid = "-"
        IF ASC(location(seedX, seedY, level).grid) > 127 AND ASC(location(seedX, seedY - 2, level).grid) > 127 THEN location(seedX, seedY - 1, level).grid = "-"
    NEXT seedY
NEXT seedX

'Рисуем диагональные линии'

FOR seedX = 6 TO 18
    FOR seedY = 4 TO 16
        IF location(seedX, seedY, level).grid = "." AND (ASC(location(seedX + 1, seedY + 1, level).grid) > 127 AND ASC(location(seedX - 1, seedY - 1, level).grid) > 127) THEN
            location(seedX, seedY, level).grid = "\"
            diagnum = diagnum + 1
        END IF
        IF location(seedX, seedY, level).grid = "." AND ASC(location(seedX + 1, seedY - 1, level).grid) > 127 AND ASC(location(seedX - 1, seedY + 1, level).grid) > 127 THEN
            location(seedX, seedY, level).grid = "/"
            diagnum = diagnum + 1
        END IF
    NEXT seedY
NEXT seedX

'Очистка лишних / и \'
FOR k = 1 TO diagnum - 1 '-количество оставшихся диагональных линий'
    DO
        seedX = INT(RND * 12) + 6
        seedY = INT(RND * 12) + 4
    LOOP UNTIL location(seedX, seedY, level).grid = "/" OR location(seedX, seedY, level).grid = "\"
    location(seedX, seedY, level).grid = "."
NEXT k



'Заблокированные переходы'
FOR blockway = 1 TO 3
    DO
        seedX = INT(RND * 12) + 6
        seedY = INT(RND * 12) + 4
    LOOP UNTIL location(seedX, seedY, level).grid = "-" OR location(seedX, seedY, level).grid = "|"
    location(seedX, seedY, level).grid = "+"
NEXT blockway


END SUB

SUB loadgame

OPEN "game.txt" FOR INPUT AS #1

INPUT #1, gamename$, levelname$

CLOSE #1


END SUB

SUB parscom

'Преобразуем все регистры в строчные
lowparstext$ = LTRIM$(RTRIM$(LCASE$(parstext$)))

FOR letter = 1 TO LEN(lowparstext$)
    l = ASC(MID$(lowparstext$, letter, 1))
    SELECT CASE l
        CASE 192 TO 207 'А-П по таблице cp1251
            l = l + 32
        CASE 208 TO 223 'Р-Я
            l = l + 32
    END SELECT
    MID$(lowparstext$, letter, 1) = CHR$(l)
NEXT letter

SELECT CASE lowparstext$

    CASE "и", "инв", "инвентарь"
        outtext$ = "У вас ничего нет"
        lowparstext$ = "Инвентарь" 'общее слово для отображения в истории команд

    CASE "счет", "очки"
        outtext$ = "В сумме" + STR$(gameturn + 1) + " ходов вы набрали очков."
        lowparstext$ = "счет"

    CASE "с", "сев", "север"

        IF location(xmap - 1, ymap, level).grid = "+" THEN
            outtext$ = "Путь на север заблокирован."

        ELSEIF location(xmap - 1, ymap, level).grid = "|" THEN
            location(xmap - 2, ymap, level).curloc = location(xmap - 2, ymap, level).grid
            location(xmap, ymap, level).curloc = ""
            xmap = xmap - 2
            CALL screenmap

        ELSE
            outtext$ = "На север не пройти."
        END IF

        lowparstext$ = "север"


    CASE "ю", "юг"

        IF location(xmap + 1, ymap, level).grid = "+" THEN
            outtext$ = "Путь на юг заблокирован."

        ELSEIF location(xmap + 1, ymap, level).grid = "|" THEN
            location(xmap + 2, ymap, level).curloc = location(xmap + 2, ymap, level).grid
            location(xmap, ymap, level).curloc = ""
            xmap = xmap + 2
            CALL screenmap

        ELSE
            outtext$ = "На юг не пройти."
        END IF
        lowparstext$ = "юг"

    CASE "в", "вос", "восток"

        IF location(xmap, ymap + 1, level).grid = "+" THEN
            outtext$ = "Путь на восток заблокирован."

        ELSEIF location(xmap, ymap + 1, level).grid = "-" THEN
            location(xmap, ymap + 2, level).curloc = location(xmap, ymap + 2, level).grid
            location(xmap, ymap, level).curloc = ""
            ymap = ymap + 2
            CALL screenmap

        ELSE
            outtext$ = "На восток не пройти."
        END IF
        lowparstext$ = "восток"


    CASE "з", "зап", "запад"

        IF location(xmap, ymap - 1, level).grid = "+" THEN
            outtext$ = "Путь на запад заблокирован."

        ELSEIF location(xmap, ymap - 1, level).grid = "-" THEN
            location(xmap, ymap - 2, level).curloc = location(xmap, ymap - 2, level).grid
            location(xmap, ymap, level).curloc = ""
            ymap = ymap - 2
            CALL screenmap

        ELSE
            outtext$ = "На запад не пройти."
        END IF
        lowparstext$ = "запад"


    CASE "св", "северо-восток"

        IF location(xmap - 1, ymap + 1, level).grid = "/" THEN
            location(xmap - 2, ymap + 2, level).curloc = location(xmap - 2, ymap + 2, level).grid
            location(xmap, ymap, level).curloc = ""
            xmap = xmap - 2
            ymap = ymap + 2
            CALL screenmap

        ELSE
            outtext$ = "На северо-восток не пройти."
        END IF
        lowparstext$ = "северо-восток"


    CASE "юв", "юго-восток"

        IF location(xmap + 1, ymap + 1, level).grid = "\" THEN
            location(xmap + 2, ymap + 2, level).curloc = location(xmap + 2, ymap + 2, level).grid
            location(xmap, ymap, level).curloc = ""
            xmap = xmap + 2
            ymap = ymap + 2
            CALL screenmap

        ELSE
            outtext$ = "На юго-восток не пройти."
        END IF
        lowparstext$ = "юго-восток"


    CASE "юз", "юго-запад"

        IF location(xmap + 1, ymap - 1, level).grid = "/" THEN
            location(xmap + 2, ymap - 2, level).curloc = location(xmap + 2, ymap - 2, level).grid
            location(xmap, ymap, level).curloc = ""
            xmap = xmap + 2
            ymap = ymap - 2
            CALL screenmap

        ELSE
            outtext$ = "На юго-запад не пройти."
        END IF
        lowparstext$ = "юго-запад"


    CASE "сз", "северо-запад"

        IF location(xmap - 1, ymap - 1, level).grid = "\" THEN
            location(xmap - 2, ymap - 2, level).curloc = location(xmap - 2, ymap - 2, level).grid
            location(xmap, ymap, level).curloc = ""
            xmap = xmap - 2
            ymap = ymap - 2
            CALL screenmap

        ELSE
            outtext$ = "На северо-запад не пройти."
        END IF
        lowparstext$ = "северо-запад"


    CASE ELSE
        outtext$ = "Команда не понятна."
        GOTO screentext 'не заносим команду в историю команд

END SELECT

comhist(0) = lowparstext$ + STRING$(39 - LEN(lowparstext$), " ") 'заполяем оставшуюся строку пробелами
IF histnum < 11 THEN histnum = histnum + 1

'Проверка, есть ли такая команда в истории. Если есть заменяем ее на следующую
FOR I = 1 TO histnum
    IF comhist(I) = comhist(0) THEN
        FOR k = I TO histnum
            comhist(k) = comhist(k + 1)
        NEXT k
        histnum = histnum - 1
        EXIT FOR 'дальше не считаем, т.к. возможно только одно совпадение
    END IF

NEXT I

'Запись истории валидных команд
FOR I = 10 TO 0 STEP -1
    comhist(I + 1) = comhist(I)
NEXT I


screentext:
gameturn = gameturn + 1
OPEN "log.txt" FOR APPEND AS #1
PRINT #1, outtext$
PRINT #1, " "
maxstring = maxstring + 2
CLOSE #1


CALL screentext

'Очищаем экран ввода
parstext$ = STRING$(39, " ")
LOCATE 24, 23: PRINT parstext$;
END SUB

SUB parser

COLOR 15
LOCATE 24, 20: PRINT ">>"; " ";

curY = 23 'положение курсора

maxstring = 0

pgupdn = 0

insert$ = "ON" 'значение кнопки insert по-умолчанию

parstext$ = STRING$(39, " ")

num = 0 'счетчик обращений к истории команд

DO

    COLOR 7

    'КУРСОР 
    curtime! = TIMER
    strtime$ = STR$(curtime!)
    IF MID$(strtime$, 8, 1) = "0" THEN
        LOCATE 24, curY: PRINT CHR$(22);
    ELSEIF MID$(strtime$, 8, 1) = "5" THEN
        LOCATE 24, curY: PRINT MID$(parstext$, curY - 22, 1);
    END IF

    'ВВОД КОМАНД

    k$ = INKEY$
    IF k$ <> "" THEN GOSUB keyinput
LOOP

keyinput:

SELECT CASE k$

    'ВЫХОД
    CASE CHR$(27)
        IF maxstring <> 0 THEN KILL "log.txt"
        END

        'СЕВЕР, 8
    CASE CHR$(56)
        parstext$ = "север" 'направлениея: пробелы не добавляем, т.к. вывод идет сразу в окно вывода
        GOSUB inputcom


        'ЮГ, 2
    CASE CHR$(50)
        parstext$ = "юг"
        GOSUB inputcom


        'ЗАПАД, 4
    CASE CHR$(52)
        parstext$ = "запад"
        GOSUB inputcom


        'ВОСТОК, 6
    CASE CHR$(54)
        parstext$ = "восток"
        GOSUB inputcom


        'СЕВЕРО-ВОСТОК, 9
    CASE CHR$(57)
        parstext$ = "северо-восток"
        GOSUB inputcom

        'ЮГО-ВОСТОК,3
    CASE CHR$(51)
        parstext$ = "юго-восток"
        GOSUB inputcom

        'ЮГО-ЗАПАД, 1
    CASE CHR$(49)
        parstext$ = "юго-запад"
        GOSUB inputcom

        'СЕВЕРО-ЗАПАД, 7
    CASE CHR$(55)
        parstext$ = "северо-запад"
        GOSUB inputcom


        'ВВОД

    CASE CHR$(13)
        inputcom:
        OPEN "log.txt" FOR APPEND AS #1
        PRINT #1, "> " + parstext$
        CLOSE #1
        maxstring = maxstring + 1
        pgupdn = 0

        num = 0

        CALL parscom 'проверяем на соответствие командам после ввода
        CALL sidescreen 'обновляем вспомогательный экран

        curY = 23
        RETURN


        'PAGE UP - промотка страницы вверх
    CASE CHR$(0) + CHR$(73)
        IF maxstring - pgupdn > 17 THEN

            pgupdn = pgupdn + 1
            CALL screentext

        END IF

        'PAGE DOWN - промотка страницы вниз
    CASE CHR$(0) + CHR$(81)
        IF maxstring + pgupdn > 17 AND pgupdn > 0 THEN

            pgupdn = pgupdn - 1
            CALL screentext

        END IF

        'CTRL-END к текущей позиции в тексте
    CASE CHR$(0) + CHR$(117)
        IF maxstring <> 0 THEN 'проверка наличия файла log.txt
            pgupdn = 0
            CALL screentext

        END IF

        'CTRL-HOME в начало истории
    CASE CHR$(0) + CHR$(119)
        IF maxstring - 17 > 0 THEN
            pgupdn = maxstring - 17
            CALL screentext

        END IF

        'BACKSPACE
    CASE CHR$(8)
        IF curY > 23 AND curY < 62 THEN
            parstext$ = LEFT$(parstext$, curY - 24) + RIGHT$(parstext$, 62 - curY) + " "

            curY = curY - 1
        END IF

        'CTRL-DELETE - очистить строку ввода
    CASE CHR$(0) + CHR$(147)
        parstext$ = STRING$(39, " ")
        curY = 23


        'TAB - переключение режима вспомогательного окна
    CASE CHR$(9)
        sidemode = sidemode + 1
        IF sidemode = 5 THEN sidemode = 1
        CALL sidescreen

        'СТРЕЛКА ВЛЕВО <-
    CASE CHR$(0) + CHR$(75)
        IF curY > 23 THEN
            IF ASC(MID$(parstext$, curY - 23, 1)) < 23 THEN
                LOCATE 24, curY: PRINT " ";
            ELSE
                LOCATE 24, curY: PRINT MID$(parstext$, curY - 22, 1);
            END IF
            curY = curY - 1
        END IF

        'СТРЕЛКА ВПРАВО ->
    CASE CHR$(0) + CHR$(77)
        IF curY < 61 THEN
            IF ASC(MID$(parstext$, curY - 22, 1)) < 23 THEN
                LOCATE 24, curY: PRINT " ";
            ELSE
                LOCATE 24, curY: PRINT MID$(parstext$, curY - 22, 1);
            END IF
            curY = curY + 1
        END IF


        'СТРЕЛКА ВВЕРХ - история команд (от новых к старым)
    CASE CHR$(0) + CHR$(72)

        IF histnum <> 0 THEN
            IF num < histnum THEN
                num = num + 1
            ELSE 'возвращаемся к началу
                num = 1
            END IF
            parstext$ = comhist(num)
            curY = LEN(RTRIM$(parstext$)) + 23
        END IF


        'СТРЕЛКА ВНИЗ - история команд (от старых к новым)
    CASE CHR$(0) + CHR$(80)

        IF histnum <> 0 THEN
            IF num > 1 THEN
                num = num - 1
                parstext$ = comhist(num)
            ELSE 'к самой старой команде
                parstext$ = comhist(histnum)
                num = histnum
            END IF
            curY = LEN(RTRIM$(parstext$)) + 23
        END IF


        'HOME - в начало строки
    CASE CHR$(0) + CHR$(71)
        curY = 23


        'END - в конец строки
    CASE CHR$(0) + CHR$(79)
        curY = LEN(RTRIM$(parstext$)) + 23


        'DELETE
    CASE CHR$(0) + CHR$(83)
        parstext$ = LEFT$(parstext$, curY - 23) + RIGHT$(parstext$, 61 - curY) + " "


        'INSERT
    CASE CHR$(0) + CHR$(82)
        IF insert$ = "ON" THEN
            insert$ = "OFF"
        ELSE insert$ = "ON"
        END IF


    CASE ELSE

        IF curY < 61 AND LEN(k$) = 1 THEN 'правая граница ввода и исключаем вывод 2-байтных символов функциональных клавиш
            IF insert$ = "OFF" OR MID$(parstext$, curY - 22, 1) = " " THEN 'если под курсором пробел, то печатаем в него, а не перескакиваем
                MID$(parstext$, curY - 22, 1) = k$
            ELSEIF insert$ = "ON" AND LEN(RTRIM$(parstext$)) < 38 THEN
                parstext$ = LEFT$(parstext$, curY - 23) + k$ + RIGHT$(parstext$, 62 - curY)
                parstext$ = LEFT$(parstext$, 39)
            ELSE 'курсор не движется вперед, если достигнут предел строки
                curY = curY - 1
            END IF

            curY = curY + 1

        END IF

END SELECT

COLOR 7
LOCATE 24, 23: PRINT parstext$;
RETURN

END SUB

SUB screenmap

location(xmap, ymap, level).playermap = location(xmap, ymap, level).curloc

'показываем выходы из локации
FOR x = -1 TO 1
    FOR y = -1 TO 1

        location(xmap + x, ymap + y, level).playermap = location(xmap + x, ymap + y, level).grid
NEXT y, x


'Задаем цвет объектам на карте
FOR seedY = 2 TO 18
    FOR seedX = 4 TO 20

        SELECT CASE location(seedX, seedY, level).playermap
            CASE "."
                COLOR 8, 0
            CASE "|", "/", "\", "-"
                COLOR 15, 0
            CASE "+"
                COLOR 8, 0
            CASE location(seedX, seedY, level).curloc
                COLOR 14, 14
            CASE ELSE
                COLOR 7, 0

        END SELECT

        LOCATE seedX, seedY: PRINT location(seedX, seedY, level).playermap
NEXT seedX, seedY

END SUB

SUB screentext

curstring = 0
nstring = 0
FOR x = 5 TO 21
    FOR y = 21 TO 61
        LOCATE x, y: PRINT " "
NEXT y, x

OPEN "log.txt" FOR INPUT AS #1
DO WHILE NOT EOF(1)
    INPUT #1, scrtext$
    curstring = curstring + 1
    IF (curstring + pgupdn > maxstring - 17) AND (nstring < 16) THEN 'РїРѕСЃР»Рµ and - РѕРіСЂР°РЅРёС‡РµРЅРёРµ РїРѕ РЅРёР¶РЅРµРјСѓ РєСЂР°СЋ СЌРєСЂР°РЅР°

        nstring = nstring + 1

        IF MID$(scrtext$, 1, 1) = ">" THEN COLOR 15 'после and - ограничение по нижнему краю экрана
        LOCATE 4 + nstring, 21: PRINT scrtext$
        COLOR 7
    END IF

    'Создаем строки вверх/вниз для индикации промотки текста
    IF curstring > 17 AND pgupdn <> maxstring - 17 THEN 'стрелка вверх
        LOCATE 5, 61: PRINT CHR$(24)
    END IF
    IF pgupdn > 0 THEN 'стрелка вниз
        LOCATE 21, 61: PRINT CHR$(25)
    END IF


LOOP
CLOSE #1

END SUB

SUB sidescreen

'Очистка вспомогательного экрана
FOR x = 63 TO 79
    LOCATE 2, x: PRINT " "
    FOR y = 4 TO 24
        LOCATE y, x: PRINT " ";
NEXT y, x

COLOR 15
LOCATE 2, 63: PRINT CHR$(17)
LOCATE 2, 79: PRINT CHR$(16)

SELECT CASE sidemode

    CASE 1
        LOCATE 2, 67: PRINT "Управление"

    CASE 2
        LOCATE 2, 67: PRINT "Инвентарь"

    CASE 3
        LOCATE 2, 68: PRINT "Легенда"

    CASE 4
        LOCATE 2, 67: PRINT "Статистика"
        COLOR 7
        LOCATE 5, 69: PRINT "Ходов:"
        LOCATE 6, 70: PRINT gameturn

END SELECT


END SUB

 
